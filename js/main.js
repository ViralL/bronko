	 var owl = $('.products-carousel');
      owl.owlCarousel({
        loop: true,
        items: 1,
        autoplayTimeout: 15000,
        dots: true
      })
      var owlinnertext = $('.owlinnertext');
      owlinnertext.owlCarousel({
        loop: true,
        items: 1,
        autoplay: true,
        dots: true
      })
    //   var owlparnters = $('.parnters-wrapper');
    //   owlparnters.owlCarousel({
    //     items: 5,
    //     margin: 50,
		// responsive : {
		//     320 : {
		//         items : 1,
		//         dots : true,
		//     },
		//     480 : {
		//         items : 3,
		//         dots : true,
		//     },
		//     992 : {
		//         items : 5,
		//         dots : false,
		//     }
		// }
    //   })
      $(document).ready(function() {

        $(window).scroll(function(){
            if ($(window).scrollTop() >= 250) {
               $('.header-site--fixed').addClass('show');
            }
            else {
               $('.header-site--fixed').removeClass('show');
            }
        });

        $(function() {
          var $elem = $('.main-wrapper');
          $('#nav_down').show('slow');
          $(".anchor").on("click","#nav_up", function (event) {
              event.preventDefault();
              $('html, body').animate({scrollTop: '0px'}, 800);
          });
          $(".anchor").on("click","#nav_down", function (event) {
              event.preventDefault();
              $('html, body').animate({scrollTop: $elem.height()}, 800);
          });
          $(window).scroll(function(event){
             if ($(this).scrollTop()>500){
               $('#nav_up').show('slow');
               $('#nav_down').hide();
             } else {
               $('#nav_down').show('slow');
               	$('#nav_up').hide();
             }
            });
         });

         $('.popover')
         .mouseover(function() {
            var popover = $(this).data('popover');
            $('.popover-main').css('left', event.pageX + 30);
            $('.popover-main').css('top', event.pageY - 30);
            $('.popover-main').html('<div class="popover-body">'+popover+'</div>')
             console.log(popover);
          })
          .mouseout(function() {
            $('.popover-main').html('');
          });

      		$('.dropdown-catalog > P').click(function() {
      			$(this).parent().find('.catalog-container-wrapper').stop(true).slideToggle();
      			$(this).toggleClass('opened');
      		});

      		$('.product-gallery a').simpleLightbox();

      		$('a.zoom-map').simpleLightbox();

      		$('.single-news-gallery a').simpleLightbox();

			$('.catalog-1-level-column').on( 'click', '.catalog-1-level-block > .line', function() {
				$(this).parent().toggleClass('height-auto');
				return false;
			});

			$('.catalog-3-level-description').on( 'click', '.show-more-text', function() {
				$(this).parent().find('.hide-text').slideDown();
				$(this).parent().toggleClass('show-text');
				return false;
			});

			$('.show-filter').click(function() {
				$('.catalog-product-filter').slideToggle();

				var buttonText = $(this).text();

 				$(this).text($(this).text() == 'Показать фильтр' ? 'Скрыть фильтр' : 'Показать фильтр');

				return false;
			});

			$('.tooltip').tooltipster({
    			contentCloning: true,
    			maxWidth: 320,
    			side: ['right', 'left', 'top', 'bottom']
			});

			$( ".slider" ).slider({
			         animate: true, // Анимация. true - включить. false - выключить.
			         min: 1, // Минимальный интервал диапазона.
			         max: 50000, // Максимальный интервал диапазона.
			         range: true, // Включение двойного ползунка. Если место true поставить 'min', то будет один ползунок.
			         step: 1, // Шаг ползунка.
			         values: [1, 50000], // Значения для ползунков. Для первого и второго.
			         slide: function(event, ui) { // Действия которые будут происходить по перетаскивания ползунка.
			         	$( "#left_count" ).html(ui.values[ 0 ]); // Значение первого ползунка.
			         	$( "#right_count" ).html(ui.values[ 1 ]); // Значение второго ползунка.
			         	$( "#price_start" ).val(ui.values[ 0 ]);
			         	$( "#price_end" ).val(ui.values[ 1 ]);
			         }
			});

			$("#price_end").change(function() {
				inputVal = $("#price_start").val();
				inputEndVal = $('#price_end').val();

			  	$( ".slider" ).slider("values", [inputVal, inputEndVal]);

			  	$("#left_count").html(inputVal);
			  	$("#right_count").html(inputEndVal);
			});

			$("#price_start").change(function() {
				inputVal = $("#price_start").val();
				inputEndVal = $('#price_end').val();

			  	$( ".slider" ).slider("values", [inputVal, inputEndVal]);

			  	$("#left_count").html(inputVal);
			  	$("#right_count").html(inputEndVal);
			});

			$('#tabs-nav li:first-child').addClass('active');
			$('.tab-content').hide();
			$('.tab-content:first').show();

			$('#cart-head a').click(function(){
				$('#cart-head a').removeClass('active');
				$(this).addClass('active');
				$('.tab-content').hide();

				var activeTab = $(this).attr('href');
				$(activeTab).fadeIn();
			  	return false;
			});

			$('.item-num-arrows > div').on('click', function() {
			  var input = $(this).closest('.item-num').find('input'); // инпут
			  var value = parseInt(input.val()) || 0; // получаем value инпута или 0
			  if ($(this).hasClass('arrow-down')) {
			  if(value == 0) {
			  	return false;
			  }
			    value = value - 1; // вычитаем из value 1
			  }
			  if ($(this).hasClass('arrow-up')) {
			    value = value + 1; // прибавляем к value 1
			  }
			  input.val(value).change(); // выводим полученное value в инпут; триггер .change() - на случай, если на изменение этого инпута у вас уже объявлен еще какой-то обработчик
			});

			$('.register-form').on('click', '.show-password-text', function(){

				var inputPassword = $(this).parents('.spare-input-group').find('input'),
					inputType = inputPassword.attr('type');


				$('.password-input').attr('type', 'text');

				$(this).text('скрыть текст пароля');
				$(this).addClass('hide-password-text');
				$(this).removeClass('show-password-text');

				return false;
			});
			$('.register-form').on('click', '.hide-password-text', function(){

				var inputPassword = $(this).parents('.spare-input-group').find('input'),
					inputType = inputPassword.attr('type');


				$('.password-input').attr('type', 'password');

				$(this).text('показать текст пароля');
				$(this).addClass('show-password-text');
				$(this).removeClass('hide-password-text');

				return false;
			});

      });

      if ($('#cbp-vm').length) {
        (function() {

            var container = document.getElementById( 'cbp-vm' ),
                optionSwitch = Array.prototype.slice.call( container.querySelectorAll( 'div.cbp-vm-options > a' ) );

            function init() {
                optionSwitch.forEach( function( el, i ) {
                    el.addEventListener( 'click', function( ev ) {
                        ev.preventDefault();
                        _switch( this );
                    }, false );
                } );
            }

            function _switch( opt ) {
                optionSwitch.forEach(function(el) {
                  $(container).removeClass(el.getAttribute( 'data-view' ));
                  $(el).removeClass('cbp-vm-selected');
                });

                $(container).addClass(opt.getAttribute( 'data-view' ));
                $(opt).addClass('cbp-vm-selected');
            }

            init();

        })();
      }
